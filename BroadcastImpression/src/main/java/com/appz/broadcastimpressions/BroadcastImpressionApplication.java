package com.appz.broadcastimpressions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages="com.appz.broadcastimpressions.repository")
@EntityScan(basePackages={"com.appz.broadcastimpressions.jpa"})
@ComponentScan(basePackages={"com.appz.broadcastimpressions.controller","com.appz.broadcastimpressions.model","com.appz.broadcastimpressions.utils"})
//@EnableWebMvc
@Configuration
@SpringBootApplication
public class BroadcastImpressionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BroadcastImpressionApplication.class, args);
	}

}
