package com.appz.broadcastimpressions.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.appz.broadcastimpressions.model.FCMNotificationContentModel;
import com.appz.broadcastimpressions.model.FCMNotificationModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class FilesUtil {
	// private static final String file = "bulkUpload.properties";
	private static Properties props = null;
	private static String fileName = "application.properties";
	private static void loadPropertiesFromClasspath() throws IOException {
		props = new Properties();
		InputStream inputStream = FilesUtil.class.getClassLoader()
				.getResourceAsStream(fileName);

		if (inputStream == null) {
			throw new FileNotFoundException("Property file 'bulkUpload.properties' not found in the classpath");
		}

		props.load(inputStream);
	}
	
	
	public static void sendNotification(List<String> tokenList,String message) throws IOException {
		String fcmUrl = FilesUtil.getProperty("fcmUrl");
		Gson gson = new GsonBuilder().serializeNulls().create();

		FCMNotificationModel notification = new FCMNotificationModel();
		notification.setRegistration_ids(tokenList);
		FCMNotificationContentModel content = new FCMNotificationContentModel();
		content.setTitle("PFM");
		content.setBody(message);
		content.setClick_action(null);
		content.setSound("default");
		notification.setNotification(content);

		URL url = new URL(fcmUrl);
//		System.out.println(gson.toJson(notification));
//		System.out.println("url "+url);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization","key="+FilesUtil.getProperty("fcmKey").trim());
		conn.setRequestProperty("Content-Type","application/json");
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(gson.toJson(notification));
		wr.flush();
//		System.out.println("Push Response Code "+conn.getResponseCode());
	
		conn.getInputStream();
		 BufferedReader br = new BufferedReader(new InputStreamReader(
                 (conn.getInputStream())));

         String output;
//         System.out.println("Output from Server .... \n");
         while ((output = br.readLine()) != null) {
             System.out.println(output);
         }
		}

	public static String getProperty(String key) {
		if (props == null) {
			try {
				loadPropertiesFromClasspath();
			} catch (Exception IOException) {
				return null;
			}
		}
		return props.getProperty(key);
	}

	public  String getProperty(String key, String defaultValue) {
		if (props == null) {
			try {
				loadPropertiesFromClasspath();
			} catch (Exception IOException) {
				return null;
			}
		}
		return props.getProperty(key, defaultValue);
	}

	public int getIntProperty(String key) {
		String property = getProperty(key);

		if (property == null) {
			return -1;
		}
		try {
			return Integer.parseInt(property);
		} catch (NumberFormatException nfe) {
			return (Integer) null;
		}
	}
}
