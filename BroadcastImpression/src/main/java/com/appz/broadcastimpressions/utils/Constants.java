package com.appz.broadcastimpressions.utils;

public class Constants {
	
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_CSV = "text/csv";
	public static final String CHARACTER_ENCODING_UTF_8 = "UTF-8";
	
	
	public static final String DRAFT_TEMPLATE_ALREADY_EXISTS ="Draft Template Already Exists";
	public static final String DRAFT_TEMPLATE_DOES_NOT_EXISTS ="Draft Template Does Not Exists";
	public static final String LOG_START = "-------------------------------------- START --------------------------------------";
	public static final String LOG_END = "----------------------------------------- END ---------------------------------------";
	public static final String REQUEST_COMPLETED = "REQUEST COMPLETED";
	
}
