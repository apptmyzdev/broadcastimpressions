package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.FeedbackModulesEntity;

/**
 * Repository : FeedbackModules.
 */
public interface FeedbackModulesJpaRepository extends PagingAndSortingRepository<FeedbackModulesEntity, Long> {

}
