package com.appz.broadcastimpressions.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.DraftModulesEntity;


/**
 * Repository : DraftModules.
 */
public interface DraftModulesJpaRepository extends PagingAndSortingRepository<DraftModulesEntity, Long> {
	
	List<DraftModulesEntity> findByDraftTemplateTemplateId(Long templateId);

}

