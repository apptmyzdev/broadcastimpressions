package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.ReminderEntity;

/**
 * Repository : Reminder.
 */
public interface ReminderJpaRepository extends PagingAndSortingRepository<ReminderEntity, Long> {

}
