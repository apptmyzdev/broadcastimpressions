package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.UsersEntity;

/**
 * Repository : Users.
 */
public interface UsersJpaRepository extends PagingAndSortingRepository<UsersEntity, Long> {

}
