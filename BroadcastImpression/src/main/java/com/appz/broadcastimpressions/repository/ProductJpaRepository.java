package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.ProductEntity;

/**
 * Repository : Product.
 */
public interface ProductJpaRepository extends PagingAndSortingRepository<ProductEntity, Long> {

}
