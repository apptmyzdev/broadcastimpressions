package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.CustomerEntity;


/**
 * Repository : Customer.
 */
public interface CustomerJpaRepository extends PagingAndSortingRepository<CustomerEntity, Long> {

}
