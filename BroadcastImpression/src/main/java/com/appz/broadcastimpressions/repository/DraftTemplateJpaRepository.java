package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.DraftTemplateEntity;


/**
 * Repository : DraftTemplate.
 */
public interface DraftTemplateJpaRepository extends PagingAndSortingRepository<DraftTemplateEntity, Long> {
	
	public DraftTemplateEntity findByTemplateName(String templateName);

	DraftTemplateEntity findBytemplateId(Long templateId);
}
