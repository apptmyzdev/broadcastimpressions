package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.CampaignEntity;


/**
 * Repository : Campaign.
 */
public interface CampaignJpaRepository extends PagingAndSortingRepository<CampaignEntity, Long> {

}
