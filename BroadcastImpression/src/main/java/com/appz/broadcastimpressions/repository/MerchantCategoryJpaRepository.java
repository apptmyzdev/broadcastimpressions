package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.MerchantCategoryEntity;

/**
 * Repository : MerchantCategory.
 */
public interface MerchantCategoryJpaRepository extends PagingAndSortingRepository<MerchantCategoryEntity, Long> {

}
