package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.FeedbackTemplateEntity;

/**
 * Repository : FeedbackTemplate.
 */
public interface FeedbackTemplateJpaRepository extends PagingAndSortingRepository<FeedbackTemplateEntity, Long> {

}
