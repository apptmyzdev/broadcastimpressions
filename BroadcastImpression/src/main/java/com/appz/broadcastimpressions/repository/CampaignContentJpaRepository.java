package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.CampaignContentEntity;
import com.appz.broadcastimpressions.jpa.CampaignContentEntityKey;




/**
 * Repository : CampaignContent.
 */
public interface CampaignContentJpaRepository extends PagingAndSortingRepository<CampaignContentEntity, CampaignContentEntityKey> {

}
