package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.appz.broadcastimpressions.jpa.CustomerReviewEntity;


/**
 * Repository : CustomerReview.
 */
public interface CustomerReviewJpaRepository extends PagingAndSortingRepository<CustomerReviewEntity, String> {

}
