package com.appz.broadcastimpressions.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.appz.broadcastimpressions.jpa.MerchantEntity;

/**
 * Repository : Merchant.
 */
public interface MerchantJpaRepository extends PagingAndSortingRepository<MerchantEntity, Long> {

}
