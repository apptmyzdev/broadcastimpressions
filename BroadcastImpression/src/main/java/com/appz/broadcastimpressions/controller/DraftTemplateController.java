package com.appz.broadcastimpressions.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appz.broadcastimpressions.jpa.DraftModulesEntity;
import com.appz.broadcastimpressions.jpa.DraftTemplateEntity;
import com.appz.broadcastimpressions.model.DraftModulesDataModel;
import com.appz.broadcastimpressions.model.DraftTemplateRequestDataModel;
import com.appz.broadcastimpressions.repository.DraftModulesJpaRepository;
import com.appz.broadcastimpressions.repository.DraftTemplateJpaRepository;
import com.appz.broadcastimpressions.utils.Constants;
import com.appz.broadcastimpressions.utils.GeneralResponse;



@RestController
@RequestMapping("/api/templates")
public class DraftTemplateController {
	
	@Autowired
	private DraftTemplateJpaRepository draftTemplateJpaRepository;
	
	@Autowired
	private DraftModulesJpaRepository draftModulesJpaRepository;
	
    /*********************** DRAFT TEMPLATE SAVE API START**************************/
	
	@PostMapping("/draft")
	public ResponseEntity<GeneralResponse> saveDraft(@RequestBody DraftTemplateRequestDataModel dataModel){
		Logger logger = Logger.getLogger("saveDraftTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findByTemplateName(dataModel.getTemplateName());
			if(draftTemplateEntity == null)
			{
				draftTemplateEntity = new DraftTemplateEntity();
				draftTemplateEntity.setTemplateName(dataModel.getTemplateName());
				draftTemplateEntity.setCreatedBy(dataModel.getCreatedBy());
				draftTemplateEntity.setCreatedOn(new Date());
				draftTemplateJpaRepository.save(draftTemplateEntity);
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								null, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_ALREADY_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	/*********************** DRAFT TEMPLATE SAVE API END**************************/
	
	
	/*********************** DRAFT TEMPLATE UPDATE API START**************************/
	@PostMapping("/draft/save")
	public ResponseEntity<GeneralResponse> updateDraftTemplate(@RequestBody DraftTemplateRequestDataModel dataModel){
		Logger logger = Logger.getLogger("editDraftTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findBytemplateId(dataModel.getTemplateId());
			if(draftTemplateEntity != null)
			{
				draftTemplateEntity.setTemplateName(dataModel.getTemplateName());
				draftTemplateEntity.setAuthorizedBy(dataModel.getAuthorizedBy());
				draftTemplateEntity.setAuthorizedOn(new Date());
				draftTemplateEntity.setPublishedOn(new Date());
				draftTemplateEntity.setStatus(dataModel.getStatus());
				draftTemplateEntity.setTemplateReference(dataModel.getTemplateReference());
				draftTemplateEntity.setVersion(dataModel.getVersion());
				draftTemplateJpaRepository.save(draftTemplateEntity);
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								null, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_DOES_NOT_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	/*********************** DRAFT TEMPLATE UPDATE API END **************************/
	
	
	/*********************** DRAFT TEMPLATE UPDATE PENDING API START **************************/
	
	@PostMapping("/draft/authorize")
	public ResponseEntity<GeneralResponse> authorizeTemplate(@RequestBody DraftTemplateRequestDataModel dataModel){
		Logger logger = Logger.getLogger("authorizeTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findBytemplateId(dataModel.getTemplateId());
			if(draftTemplateEntity != null)
			{
				draftTemplateEntity.setAuthorizedBy(dataModel.getAuthorizedBy());
				draftTemplateEntity.setAuthorizedOn(new Date());
				draftTemplateJpaRepository.save(draftTemplateEntity);
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								null, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_DOES_NOT_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	/*********************** DRAFT TEMPLATE UPDATE AUTHORIZE API END **************************/
	
	/*********************** DRAFT TEMPLATE UPDATE PUBLISH API START **************************/
	
	@PostMapping("/draft/publish")
	public ResponseEntity<GeneralResponse> publishTemplate(@RequestBody DraftTemplateRequestDataModel dataModel){
		Logger logger = Logger.getLogger("publishTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findBytemplateId(dataModel.getTemplateId());
			if(draftTemplateEntity != null)
			{
				draftTemplateEntity.setPublishedOn(new Date());
				draftTemplateJpaRepository.save(draftTemplateEntity);
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								null, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_DOES_NOT_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	/*********************** DRAFT TEMPLATE UPDATE AUTHORIZE API END **************************/
	
	/************************* TEMPLATE GET API START**************************/
	
	@GetMapping("/draft/{templateId}")
	public ResponseEntity<GeneralResponse> getDraftTemplate(@PathVariable Long templateId){
		Logger logger = Logger.getLogger("publishTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		DraftTemplateRequestDataModel responseModel = new DraftTemplateRequestDataModel();
		List<DraftModulesDataModel> draftModuleList = new ArrayList<DraftModulesDataModel>();
		DraftModulesDataModel draftModulesDataModel = null;
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findBytemplateId(templateId);
			if(draftTemplateEntity != null)
			{
				responseModel.setAuthorizedBy(draftTemplateEntity.getAuthorizedBy());
				responseModel.setStatus(draftTemplateEntity.getStatus());
				responseModel.setTemplateId(draftTemplateEntity.getTemplateId());
				responseModel.setTemplateName(draftTemplateEntity.getTemplateName());
				responseModel.setVersion(draftTemplateEntity.getVersion());
				responseModel.setTemplateReference(draftTemplateEntity.getTemplateReference());
				List<DraftModulesEntity> draftModulesEntities = draftModulesJpaRepository.findByDraftTemplateTemplateId(draftTemplateEntity.getTemplateId());
				if(draftModulesEntities.size()>0)
				{
					for(DraftModulesEntity d : draftModulesEntities)
					{
						draftModulesDataModel = new DraftModulesDataModel();
						draftModulesDataModel.setElements(d.getElements());
						draftModulesDataModel.setModuleId(d.getModuleId());
						draftModulesDataModel.setModuleName(d.getModuleName());
						draftModulesDataModel.setTemplateId(d.getDraftTemplate().getTemplateId());
						draftModulesDataModel.setType(d.getType());
						draftModulesDataModel.setModuleNumber(d.getModuleNo());
						draftModuleList.add(draftModulesDataModel);
					}
					if(draftModuleList.size()>0)
					{
						responseModel.setDraftModules(draftModuleList);
					}
				}
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								responseModel, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_DOES_NOT_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	/************************* TEMPLATE GET API END ***************************/
	
	
	/************************* TEMPLATE SAVE API START ***************************/
    @PostMapping("save/draft/template")
	public ResponseEntity<GeneralResponse> saveDraftTemplate(@RequestBody DraftTemplateRequestDataModel requestModel){
		Logger logger = Logger.getLogger("publishTemplate");
		ResponseEntity<GeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		List<DraftModulesDataModel> draftModuleList = new ArrayList<DraftModulesDataModel>();
		DraftModulesEntity draftModulesEntity = null;
		try
		{
			
			DraftTemplateEntity draftTemplateEntity = draftTemplateJpaRepository.findByTemplateName(requestModel.getTemplateName());
			if(draftTemplateEntity == null)
			{
				draftTemplateEntity = new DraftTemplateEntity();
				draftTemplateEntity.setAuthorizedBy(requestModel.getAuthorizedBy());
				draftTemplateEntity.setStatus(requestModel.getStatus());
				draftTemplateEntity.setTemplateId(requestModel.getTemplateId());
				draftTemplateEntity.setTemplateName(requestModel.getTemplateName());
				draftTemplateEntity.setVersion(requestModel.getVersion());
				draftTemplateEntity.setTemplateReference(requestModel.getTemplateReference());
				draftTemplateJpaRepository.save(draftTemplateEntity);
				if(requestModel.getDraftModules().size()>0)
				{
					for(DraftModulesDataModel d : requestModel.getDraftModules())
					{
						draftModulesEntity = new DraftModulesEntity();
						draftModulesEntity.setElements(d.getElements());
						draftModulesEntity.setModuleId(d.getModuleId());
						draftModulesEntity.setModuleName(d.getModuleName());
						draftModulesEntity.setDraftTemplate(draftTemplateEntity);
						draftModulesEntity.setType(d.getType());
						draftModulesEntity.setModuleNo(d.getModuleNumber());
					    draftModulesJpaRepository.save(draftModulesEntity);
					}
				}
				generalResponse = new ResponseEntity<GeneralResponse>(
						new GeneralResponse(HttpServletResponse.SC_OK, Constants.REQUEST_COMPLETED,
								null, null),
						HttpStatus.OK);
			}
			else
			{
				generalResponse = new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.DRAFT_TEMPLATE_ALREADY_EXISTS, null, null), HttpStatus.CONFLICT);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			generalResponse =  new ResponseEntity<GeneralResponse>(new GeneralResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, null, null, null), HttpStatus.CONFLICT);
		}
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	
	
	
	
	/************************* TEMPLATE SAVE API END ***************************/
	
	
	
	
	
	
	

}
