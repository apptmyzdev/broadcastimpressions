package com.appz.broadcastimpressions.model;

import java.util.List;

public class DraftTemplateRequestDataModel {
	
	private String templateName;
	private Long createdBy;
	private Long templateId;
	private Long templateReference;
	private Long authorizedBy;
	private Integer version;
	private String status;
	private List<DraftModulesDataModel> draftModules;
	
	
	
	
	
	public List<DraftModulesDataModel> getDraftModules() {
		return draftModules;
	}
	public void setDraftModules(List<DraftModulesDataModel> draftModules) {
		this.draftModules = draftModules;
	}
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public Long getTemplateReference() {
		return templateReference;
	}
	public void setTemplateReference(Long templateReference) {
		this.templateReference = templateReference;
	}
	public Long getAuthorizedBy() {
		return authorizedBy;
	}
	public void setAuthorizedBy(Long authorizedBy) {
		this.authorizedBy = authorizedBy;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	

}
