package com.appz.broadcastimpressions.model;

import java.util.List;

public class FCMNotificationModel {
	private List<String> registration_ids;
	private FCMNotificationContentModel notification;
	private Object data;
	public FCMNotificationModel() {
		super();
	}
	
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
	public FCMNotificationContentModel getNotification() {
		return notification;
	}
	public void setNotification(FCMNotificationContentModel notification) {
		this.notification = notification;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "FCMNotificationModel [registration_ids=" + registration_ids + ", notification=" + notification
				+ ", data=" + data + "]";
	}
	
	
	
}
