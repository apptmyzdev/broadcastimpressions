package com.appz.broadcastimpressions.model;

public class DraftModulesDataModel {
	
	private Long moduleId;
	private String moduleName;
	private Integer moduleNumber;
	private String type;
	private String elements;
	private Long templateId;
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getModuleNumber() {
		return moduleNumber;
	}
	public void setModuleNumber(Integer moduleNumber) {
		this.moduleNumber = moduleNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getElements() {
		return elements;
	}
	public void setElements(String elements) {
		this.elements = elements;
	}
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	
	
	

}
