/*
 * Created on 24 Aug 2020 ( Time 16:05:32 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.appz.broadcastimpressions.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "merchant"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="merchant", catalog="reviews" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="MerchantEntity.countAll", query="SELECT COUNT(x) FROM MerchantEntity x" )
} )
public class MerchantEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @Column(name="merchant_id", nullable=false)
    private Long       merchantId   ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="merchant_name", length=100)
    private String     merchantName ;

    @Column(name="business_descrption", length=500)
    private String     businessDescrption ;

    @Column(name="licence_status", length=3)
    private String     licenceStatus ;

    @Column(name="status", length=3)
    private String     status       ;

    @Column(name="industry")
    private Integer    industry     ;

    @Column(name="category")
    private Long       category     ;

    @Column(name="facebook_handle", length=150)
    private String     facebookHandle ;

    @Column(name="instagram_handle", length=150)
    private String     instagramHandle ;

    @Column(name="linkedin_handle", length=150)
    private String     linkedinHandle ;

    @Column(name="website", length=200)
    private String     website      ;

    @Column(name="email", length=150)
    private String     email        ;

    @Column(name="primary_contact_no", length=13)
    private String     primaryContactNo ;

    @Column(name="alternative_contact_no", length=13)
    private String     alternativeContactNo ;

    @Column(name="whatsapp_business_number", length=13)
    private String     whatsappBusinessNumber ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on")
    private Date       createdOn    ;

    @Column(name="created_by")
    private Long       createdBy    ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public MerchantEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setMerchantId( Long merchantId ) {
        this.merchantId = merchantId ;
    }
    public Long getMerchantId() {
        return this.merchantId;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : merchant_name ( VARCHAR ) 
    public void setMerchantName( String merchantName ) {
        this.merchantName = merchantName;
    }
    public String getMerchantName() {
        return this.merchantName;
    }

    //--- DATABASE MAPPING : business_descrption ( VARCHAR ) 
    public void setBusinessDescrption( String businessDescrption ) {
        this.businessDescrption = businessDescrption;
    }
    public String getBusinessDescrption() {
        return this.businessDescrption;
    }

    //--- DATABASE MAPPING : licence_status ( VARCHAR ) 
    public void setLicenceStatus( String licenceStatus ) {
        this.licenceStatus = licenceStatus;
    }
    public String getLicenceStatus() {
        return this.licenceStatus;
    }

    //--- DATABASE MAPPING : status ( VARCHAR ) 
    public void setStatus( String status ) {
        this.status = status;
    }
    public String getStatus() {
        return this.status;
    }

    //--- DATABASE MAPPING : industry ( INT ) 
    public void setIndustry( Integer industry ) {
        this.industry = industry;
    }
    public Integer getIndustry() {
        return this.industry;
    }

    //--- DATABASE MAPPING : category ( BIGINT ) 
    public void setCategory( Long category ) {
        this.category = category;
    }
    public Long getCategory() {
        return this.category;
    }

    //--- DATABASE MAPPING : facebook_handle ( VARCHAR ) 
    public void setFacebookHandle( String facebookHandle ) {
        this.facebookHandle = facebookHandle;
    }
    public String getFacebookHandle() {
        return this.facebookHandle;
    }

    //--- DATABASE MAPPING : instagram_handle ( VARCHAR ) 
    public void setInstagramHandle( String instagramHandle ) {
        this.instagramHandle = instagramHandle;
    }
    public String getInstagramHandle() {
        return this.instagramHandle;
    }

    //--- DATABASE MAPPING : linkedin_handle ( VARCHAR ) 
    public void setLinkedinHandle( String linkedinHandle ) {
        this.linkedinHandle = linkedinHandle;
    }
    public String getLinkedinHandle() {
        return this.linkedinHandle;
    }

    //--- DATABASE MAPPING : website ( VARCHAR ) 
    public void setWebsite( String website ) {
        this.website = website;
    }
    public String getWebsite() {
        return this.website;
    }

    //--- DATABASE MAPPING : email ( VARCHAR ) 
    public void setEmail( String email ) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    //--- DATABASE MAPPING : primary_contact_no ( VARCHAR ) 
    public void setPrimaryContactNo( String primaryContactNo ) {
        this.primaryContactNo = primaryContactNo;
    }
    public String getPrimaryContactNo() {
        return this.primaryContactNo;
    }

    //--- DATABASE MAPPING : alternative_contact_no ( VARCHAR ) 
    public void setAlternativeContactNo( String alternativeContactNo ) {
        this.alternativeContactNo = alternativeContactNo;
    }
    public String getAlternativeContactNo() {
        return this.alternativeContactNo;
    }

    //--- DATABASE MAPPING : whatsapp_business_number ( VARCHAR ) 
    public void setWhatsappBusinessNumber( String whatsappBusinessNumber ) {
        this.whatsappBusinessNumber = whatsappBusinessNumber;
    }
    public String getWhatsappBusinessNumber() {
        return this.whatsappBusinessNumber;
    }

    //--- DATABASE MAPPING : created_on ( TIMESTAMP ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : created_by ( BIGINT ) 
    public void setCreatedBy( Long createdBy ) {
        this.createdBy = createdBy;
    }
    public Long getCreatedBy() {
        return this.createdBy;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(merchantId);
        sb.append("]:"); 
        sb.append(merchantName);
        sb.append("|");
        sb.append(businessDescrption);
        sb.append("|");
        sb.append(licenceStatus);
        sb.append("|");
        sb.append(status);
        sb.append("|");
        sb.append(industry);
        sb.append("|");
        sb.append(category);
        sb.append("|");
        sb.append(facebookHandle);
        sb.append("|");
        sb.append(instagramHandle);
        sb.append("|");
        sb.append(linkedinHandle);
        sb.append("|");
        sb.append(website);
        sb.append("|");
        sb.append(email);
        sb.append("|");
        sb.append(primaryContactNo);
        sb.append("|");
        sb.append(alternativeContactNo);
        sb.append("|");
        sb.append(whatsappBusinessNumber);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(createdBy);
        return sb.toString(); 
    } 

}
