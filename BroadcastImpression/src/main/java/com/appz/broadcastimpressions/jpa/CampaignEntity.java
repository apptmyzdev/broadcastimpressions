/*
 * Created on 24 Aug 2020 ( Time 16:05:29 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.appz.broadcastimpressions.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "campaign"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="campaign", catalog="reviews" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="CampaignEntity.countAll", query="SELECT COUNT(x) FROM CampaignEntity x" )
} )
public class CampaignEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @Column(name="campaign_id", nullable=false)
    private Long       campaignId   ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="campaign_name", length=100)
    private String     campaignName ;

    @Column(name="created_by")
    private Long       createdBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on")
    private Date       createdOn    ;

    @Column(name="approved_by")
    private Long       approvedBy   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="approved_on")
    private Date       approvedOn   ;

    @Column(name="status", length=3)
    private String     status       ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_time")
    private Date       modifiedTime ;

    @Column(name="merchant")
    private Long       merchant     ;

    @Column(name="template")
    private Long       template     ;

    @Column(name="product")
    private Long       product      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public CampaignEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setCampaignId( Long campaignId ) {
        this.campaignId = campaignId ;
    }
    public Long getCampaignId() {
        return this.campaignId;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : campaign_name ( VARCHAR ) 
    public void setCampaignName( String campaignName ) {
        this.campaignName = campaignName;
    }
    public String getCampaignName() {
        return this.campaignName;
    }

    //--- DATABASE MAPPING : created_by ( BIGINT ) 
    public void setCreatedBy( Long createdBy ) {
        this.createdBy = createdBy;
    }
    public Long getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : created_on ( TIMESTAMP ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : approved_by ( BIGINT ) 
    public void setApprovedBy( Long approvedBy ) {
        this.approvedBy = approvedBy;
    }
    public Long getApprovedBy() {
        return this.approvedBy;
    }

    //--- DATABASE MAPPING : approved_on ( TIMESTAMP ) 
    public void setApprovedOn( Date approvedOn ) {
        this.approvedOn = approvedOn;
    }
    public Date getApprovedOn() {
        return this.approvedOn;
    }

    //--- DATABASE MAPPING : status ( VARCHAR ) 
    public void setStatus( String status ) {
        this.status = status;
    }
    public String getStatus() {
        return this.status;
    }

    //--- DATABASE MAPPING : modified_time ( TIMESTAMP ) 
    public void setModifiedTime( Date modifiedTime ) {
        this.modifiedTime = modifiedTime;
    }
    public Date getModifiedTime() {
        return this.modifiedTime;
    }

    //--- DATABASE MAPPING : merchant ( BIGINT ) 
    public void setMerchant( Long merchant ) {
        this.merchant = merchant;
    }
    public Long getMerchant() {
        return this.merchant;
    }

    //--- DATABASE MAPPING : template ( BIGINT ) 
    public void setTemplate( Long template ) {
        this.template = template;
    }
    public Long getTemplate() {
        return this.template;
    }

    //--- DATABASE MAPPING : product ( BIGINT ) 
    public void setProduct( Long product ) {
        this.product = product;
    }
    public Long getProduct() {
        return this.product;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(campaignId);
        sb.append("]:"); 
        sb.append(campaignName);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(approvedBy);
        sb.append("|");
        sb.append(approvedOn);
        sb.append("|");
        sb.append(status);
        sb.append("|");
        sb.append(modifiedTime);
        sb.append("|");
        sb.append(merchant);
        sb.append("|");
        sb.append(template);
        sb.append("|");
        sb.append(product);
        return sb.toString(); 
    } 

}
